const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

//Routes

// Add product route (admin only)
router.post("/addProduct", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .addProduct(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send({ auth: "failed" });
  }
});

// Retrieve all products route (admin only)
router.get("/allProducts", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .getAllProducts()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send({ auth: "failed" });
  }
});

//Retrieve all active products route (users)
router.get("/allActive", (req, res) => {
  productController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

//Retrieve a single product
router.get("/:productId",  (req, res) => {
  
    productController
      .getProductUser(req.params)
      .then((resultFromController) => res.send(resultFromController));
  
});

//Update product information route (admin only)
router.put("/update/:productId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .updateProduct(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" }); //Not Admin
  }
});

// //Archive product (admin only)
// router.put("/archive/:productId", auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);

//   if (userData.isAdmin) {
//     productController
//       .archiveProduct(req.params)
//       .then((resultFromController) => res.send(resultFromController));
//   } else {
//     res.send({ auth: "failed" });
//   }
// });

//Archive product
router.put('/archive/:productId', auth.verify, (req, res) => {

  const data = {
    productId : req.params.productId,
    isAdmin : auth.decode(req.headers.authorization).isAdmin
  }

  productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

// Activate product
router.put('/active/:productId', auth.verify, (req, res) => {

  const data = {
    productId : req.params.productId,
    isAdmin : auth.decode(req.headers.authorization).isAdmin
  }

  productController.activeProduct(data).then(resultFromController => res.send(resultFromController))
});



//Add to cart
router.post("/addToCart", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    res.send({ auth: "Not Allowed" });
  } else {
    let data = {
      productId: req.body.productId,
      quantity: req.body.quantity,
    };

    productController
      .addToCart(req.body)
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Delete Product
router.delete("/delete/:productId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    productController
      .deleteProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" }); //Not Admin
  }
});

module.exports = router;
