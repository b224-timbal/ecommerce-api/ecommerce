const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Controller Functions:

// Add product
module.exports.addProduct = (reqBody) => {
  return Product.find({ name: reqBody.name }).then((result) => {
    if (result.length > 0) {
      return false;
    } else {
      let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stocks: reqBody.stocks,
      });
      return newProduct.save().then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};

// Retrieve all products (Admin only)
module.exports.getAllProducts = () => {
  return Product.find({}).then((result) => {
    return result;
  });
};

// Retrieve all active products (users)
module.exports.getAllActive = () => {
  return Product.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Retrieve a single product (user)
module.exports.getProductUser = (reqParams) => {
  return Product.findById(reqParams.productId).then((result) => {
    return result;
  });
};

// Retrieve a single product (admin)
module.exports.getProductAdmin = (reqParams) => {
  return Product.findById(reqParams.productId).then((result, error) => {
    if (error) {
      return false;
    } else {
      return result;
    }
  });
};

//Update product information (admin only)
module.exports.updateProduct = (reqParams, reqBody) => {
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    stocks: reqBody.stocks,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(
    (updatedProduct, error) => {
      if (error) {
        return false;
      } else {
        let newStatus = {
          isActive: true,
        };
        return Product.findByIdAndUpdate(reqParams.productId, newStatus).then(
          (status) => {
            return true //"Product has been updated!";
          }
        );
      }
    }
  );
};

// // Archive product (admin only)
// module.exports.archiveProduct = (reqParams) => {
//   let archivedProductStatus = {
//     isActive: reqParams.isActive,
//   };

//   return Product.findByIdAndUpdate(
//     reqParams.productId,
//     archivedProductStatus
//   ).then((status, error) => {
//     console.log(`Initial Status: ${status.isActive}`);
//     if (error) {
//       return false;
//     } else if (status.isActive) {
//       let newStatus = {
//         isActive: false,
//       };
//       return Product.findByIdAndUpdate(reqParams.productId, newStatus).then(
//         (newStatus, error) => {
//           if (error) {
//             return false;
//           } else {
//             return true
//           }
//         }
//       );
//     } else if (status.isActive == false) {
//       let newStatus = {
//         isActive: true,
//       };
//       return Product.findByIdAndUpdate(reqParams.productId, newStatus).then(
//         (newStatus, error) => {
//           if (error) {
//             return false;
//           } else {
//             return true;
//           }
//         }
//       );
//     }
//   });
// };


//Archive product
module.exports.archiveProduct = (data) => {

  return Product.findById(data.productId).then((result, err) => {

    if(data.isAdmin === true) {

      result.isActive = false;

      return result.save().then((archivedProduct, err) => {

        // Product not archived
        if(err) {

          return false;

        // Product archived successfully
        } else {

          return true;
        }
      })

    } else {

      //If user is not Admin
      return false
    }

  })
};

// Activate Product
module.exports.activeProduct = (data) => {

  return Product.findById(data.productId).then((result, err) => {

    if(data.isAdmin === true) {

      result.isActive = true;

      return result.save().then((activeProduct, err) => {

        // Product not activated
        if(err) {

          return false;

        // Product activated successfully
        } else {

          return true;
        }
      })

    } else {

      //If user is not Admin
      return false
    }

  })
};



//Delete Product
module.exports.deleteProduct = (reqParams) => {
  return Product.findByIdAndDelete(reqParams.productId).then(
    (result, error) => {
      if (error) {
        return false;
      } else {
        return true;
			
      }
    }
  );
};
